let rasporedTestniUlaz=`BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30`;


function vratiBrojZaDan(stringDan){
  if (stringDan.localeCompare("ponedjeljak")==5) return 1;
  if (stringDan.localeCompare("utorak")==0) return 2;
  if (stringDan.localeCompare("srijeda")==0) return 3;
  if (stringDan.localeCompare("cetvrtak")==4) return 4;
  if (stringDan.localeCompare("petak")==0) return 5;
}

class Raspored{
   constructor(csvRaspored){ //ovdje ide let raspored, dijelti ga na /n pa na ","
    let nizEl=csvRaspored.split("\n");  //svaki raspored za sebe, idu u novi red svakako
    let i;
    this.nizRasporeda=[];

    for (i=0;i<nizEl.length;i++){
        let dijeloviRasporeda=nizEl[i].split(":");

        let nazivPredmeta=dijeloviRasporeda[0];
        let aktivnost=dijeloviRasporeda[1];
        let dan=dijeloviRasporeda[2];
        let vrijemePocetka=dijeloviRasporeda[3];
        let vrijemeKraja=dijeloviRasporeda[4];

        this.nizRasporeda.push({naziv:nazivPredmeta,aktivnost:aktivnost,dan:dan,start:vrijemePocetka,end:vrijemeKraja});
        //console.log(this.nizRasporeda[i].end); testirano, radi konstruktor odlicno
      }
     }

   dajTrenutnuAktivnost(stringNazivaGrupe,stringVremena){
    let i;
    let dijeloviPoslanogVremena=stringVremena.split("T");
    let danVremena=dijeloviPoslanogVremena[0];
    let satnicaVremena=dijeloviPoslanogVremena[1];
    let newyear=new Date(danVremena);
    for (i=0;i<this.nizRasporeda.length;i++){
      // ispravno ulazi svaki raspored u funkciju i u for petljum testirano sa : console.log(this.nizRasporeda[i].naziv);
        if (this.nizRasporeda[i].naziv.localeCompare(stringNazivaGrupe)==0 && newyear.getDay()==vratiBrojZaDan(this.nizRasporeda[i].dan)){ //naziv je ok npr bwt-grupa12, localCompare vraca bas da li su identicni a includes ne
        //testirao koliko ce se puta ispisat da znam radi li poredjenje console.log("test");

        if (satnicaVremena>=this.nizRasporeda[i].start && satnicaVremena<=this.nizRasporeda[i].end){ //https://stackoverflow.com/questions/6212305/how-can-i-compare-two-time-strings-in-the-format-hhmmss
          var timeStart = new Date("11/05/2000 " + satnicaVremena).getTime();
          var timeEnd = new Date("11/05/2000 " + this.nizRasporeda[i].end).getTime();  //https://stackoverflow.com/questions/11038252/how-can-i-calculate-the-difference-between-two-times-that-are-in-24-hour-format
          var difference = timeEnd-timeStart; 
          var resultInMinutes = Math.round(difference / 6000);
          return this.nizRasporeda[i].naziv+" "+resultInMinutes;
        }
       }
      }
      return this.nizRasporeda[i].naziv;
    }

    dajSljedecuAktivnost(stringNazivaGrupe,stringVremena){
      let i;
      let dijeloviPoslanogVremena=stringVremena.split("T");
      let danVremena=dijeloviPoslanogVremena[0];
      let satnicaVremena=dijeloviPoslanogVremena[1];
      let newyear=new Date(danVremena);
      for (i=0;i<this.nizRasporeda.length-1;i++){
    
        if (this.nizRasporeda[i].naziv.localeCompare(stringNazivaGrupe)==0 && newyear.getDay()==vratiBrojZaDan(this.nizRasporeda[i].dan)){
  
           
          if (satnicaVremena<=this.nizRasporeda[i].start){
            var timeStart = new Date("11/05/2000 " +this.nizRasporeda[i].start).getTime();
            var trenutnoTime = new Date("11/05/2000 " + satnicaVremena).getTime();
            var difference = timeStart-trenutnoTime; 
            var resultInMinutes = Math.round(difference / 60000);
            return this.nizRasporeda[i].naziv+" "+resultInMinutes;
        }
      }
    }
    return null;
}

dajPrethodnuAktivnost(stringNazivaGrupe,stringVremena){
  let i;
  let dijeloviPoslanogVremena=stringVremena.split("T");
  let danVremena=dijeloviPoslanogVremena[0];
  let satnicaVremena=dijeloviPoslanogVremena[1];
  let newyear=new Date(danVremena);
  for (i=0;i<this.nizRasporeda.length;i++){

    if (this.nizRasporeda[i].naziv.localeCompare(stringNazivaGrupe)==0 && newyear.getDay()==vratiBrojZaDan(this.nizRasporeda[i].dan)){

       //znaci nasli smo taj predmet, trenutno vrijeme nije unutar njega, vec je negdje iza njegovog kraja znaci ova aktivnost je prethodna
      if (!(satnicaVremena>=this.nizRasporeda[i].start && satnicaVremena<=this.nizRasporeda[i].end) && this.nizRasporeda[i].end<satnicaVremena){
        var timeStart = new Date("11/05/2000 " +satnicaVremena).getTime();
        var trenutnoTime = new Date("11/05/2000 " + this.nizRasporeda[i].end).getTime();
        var difference = timeStart-trenutnoTime; 
        var resultInMinutes = Math.round(difference / 6000);
        return this.nizRasporeda[i].naziv+" "+resultInMinutes;
    }
  }
}
return null;
}

}

document.getElementById("trenutna1").addEventListener("click", testiraj, false);

function testiraj(){
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="BWT";
    let time="10-12-2020T16:30:00";
    alert(r.dajTrenutnuAktivnost(predmet,time));
}


document.getElementById("trenutna2").addEventListener("click", testiraj2, false);

function testiraj2(){
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="FWT";
    let time="11-12-2020T09:10:00";
    alert(r.dajTrenutnuAktivnost(predmet,time));
}


document.getElementById("sljedeca1").addEventListener("click", testiraj3, false);

function testiraj3(){
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="BWT";
    let time="10-12-2020T14:30:00";
    alert(r.dajSljedecuAktivnost(predmet,time));
}

document.getElementById("sljedeca2").addEventListener("click", testiraj4, false);

function testiraj4(){
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="ASP";
    let time="11-11-2020T08:00:00";
    alert(r.dajSljedecuAktivnost(predmet,time));
}


document.getElementById("prethodna1").addEventListener("click", testiraj5, false);

function testiraj5(){
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="MUR2";
    let time="11-12-2020T17:00:00";
    alert(r.dajPrethodnuAktivnost(predmet,time));
}

document.getElementById("prethodna2").addEventListener("click", testiraj6, false);

function testiraj6(){
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="ASP";
    let time="11-11-2020T13:00:00";
    alert(r.dajPrethodnuAktivnost(predmet,time));
}
