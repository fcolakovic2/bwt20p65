let assert = chai.assert;
describe('GoogleMeet', function(){
  describe('#VracanjeNulla', function(){

  it('Nema nijednog predavanja', function(){
    
    let stranica = 
        `<div class=\"course-content\"><h2 class=\"accesshide\">Uvodna sekcija (sedmični format)</h2><ul class=\"weeks\"><li id=\"section-0\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-1566-title\" data-sectionid=\"0\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1566-title\" class=\"sectionname accesshide\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-0\">Opšte</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"><li class=\"activity label modtype_label \" id=\"module-72184\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><strong></strong><strong>Studenti-ponovci</strong><strong>&nbsp;</strong><span><strong>trebaju do 25.10.2020. godine</strong>&nbsp;</span>javiti (mailom) <strong>predmetnom nastavniku i asistentu</strong> proslogodisnje osvojene bodove koje zele prenijeti u tekucu akademsku godinu! Student u mailu treba <strong>obavezno navesti</strong><strong> sve podatke</strong> koji se odnose na <strong>naziv predmeta i iznos bodova za svaku aktivnost</strong>&nbsp;(iz kojih zele prenijeti bodove).<br></p></div></div></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72185\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><strong>Obavještavaju se ponovci da prenos bodova mogu uraditi samo ako je u skladu sa \"Prelazne odredbe o priznavanju poena stečenih u ranijim akademskim godinama\" - nalazi se na stranici fakulteta i Zakonom o visokom obrazovanju. Bodovi za rad na vježbama i kvizovima i bodovi na parcijalnim ispitima se mogu prenijeti samo iz prethodne akademske godine.</strong><br></p></div></div></div></div></div></div></li><li class=\"activity choice modtype_choice \" id=\"module-72276\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"\" href=\"https://c2.etf.unsa.ba/mod/choice/view.php?id=72276\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/choice/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Odabir grupa za vježbe (dodana grupa, produžen rok)<span class=\"accesshide \"> Izbor</span></span></a></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72647\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><div style=\"margin-left: 30px;color:crimson;\" class=\"editor-indent\">
            <p dir=\"ltr\" style=\"text-align: left;\"><strong>Studenti koji imaju preklapanje sa drugim predmetima i terminom vježbe iz WT-a trebaju odabrati novi termin. Otvorena je nova grupa. I studenti iz grupa koji su već odabrali, a odgovara im neki drugi, slobodan termin, mogu promijeniti grupu. Studenti koji su prenijeli bodove za prisustvo ne trebaju birati grupu.</strong><br></p>
        </div></div></div></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72628\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\">Studenti grupe ponedjeljkom (od 17:00 sati) gdje su prijavljena samo dva studenta, trebaju doći u neku drugu grupu. Kako nema dovoljno studenata u toj grupi da bi se nastava normalno izvodila, grupa se ukida. Ukoliko bude još upisanih studenata koji nemaju grupu tada ćemo otvoriti novi termin.<br></p></div></div></div></div></div></div></li></ul></div></li><li id=\"section-1\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-1793-title\" data-sectionid=\"1\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1793-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-1\">1 October - 7 October</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"><li class=\"activity label modtype_label \" id=\"module-71968\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/uca-utbm-fsv\">Link za pristup </a></p><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/jcm-tkvb-djz\">Link za pristup </a></p><p dir=\"ltr\" style=\"text-align: left;\">Za pristup je potrebno koristiti fakultetski mail.</p></div></div></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-72083\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=72083&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=72083\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/pdf-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Uvodno predavanje<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72139\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><h4 dir=\"ltr\" style=\"text-align: left;\">Molimo vas popunite formu, najkasnije do 18.10.2020&nbsp; do 12:00 sati - <a href=\"https://forms.gle/zMyNVF1qrekmTNgx8\">link</a>. (Pristup sa fakultetskim mailom)<br></h4></div></div></div></div></div></div></li></ul></div></li><li id=\"section-2\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-1794-title\" data-sectionid=\"2\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1794-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-2\">8 October - 14 October</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"><li class=\"activity label modtype_label \" id=\"module-72355\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/pdn-pvky-zyd\">Link za pristup</a></p></div></div></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72356\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/hay-xfmm-pvp\">Link za pristup</a></p></div></div></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-53453\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=53453&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=53453\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/pdf-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">HTML<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity url modtype_url \" id=\"module-39663\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/url/view.php?id=39663&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/url/view.php?id=39663\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/url/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 1 - HTML<span class=\"accesshide \"> URL adresa</span></span></a></div></div></div></div></li><li class=\"activity url modtype_url \" id=\"module-72644\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"\" href=\"https://c2.etf.unsa.ba/mod/url/view.php?id=72644\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/url/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 1 - Google Meet link<span class=\"accesshide \"> URL adresa</span></span></a></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72702\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\">Link Meet-a za vježbu biće ažuriran par minuta prije početka vježbe<br></p></div></div></div></div></div></div></li></ul></div></li><li id=\"section-3\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-1795-title\" data-sectionid=\"3\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1795-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-3\">15 October - 21 October</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"><li class=\"activity label modtype_label \" id=\"module-72800\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/bfm-jfpz-gfw\">Link za pristup </a></p></div></div></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72799\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/vjp-agsh-cgw\">Link za pristup </a></p></div></div></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-72831\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=72831&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=72831\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/pdf-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">HTML5<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-58954\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=58954&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=58954\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/pdf-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">CSS<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity url modtype_url \" id=\"module-39779\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/url/view.php?id=39779&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/url/view.php?id=39779\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/url/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 2 - CSS i layout stranice<span class=\"accesshide \"> URL adresa</span></span></a></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-47752\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=47752&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=47752\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/pdf-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 2 - Responzivni web dizajn<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity url modtype_url \" id=\"module-59102\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"\" href=\"https://c2.etf.unsa.ba/mod/url/view.php?id=59102\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/url/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 2 - Uputstvo za testiranje HTML i CSS layouta<span class=\"accesshide \"> URL adresa</span></span></a></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-47843\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=47843&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=47843\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/archive-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Primjer - Grid layout<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity quiz modtype_quiz \" id=\"module-53753\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"\" href=\"https://c2.etf.unsa.ba/mod/quiz/view.php?id=53753\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/quiz/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Kviz 1</span></a></div></div></div></div></li><li class=\"activity url modtype_url \" id=\"module-73035\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"\" href=\"https://c2.etf.unsa.ba/mod/url/view.php?id=73035\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/url/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 2 - Google Meet link<span class=\"accesshide \"> URL adresa</span></span></a></div></div></div></div></li></ul></div></li><li id=\"section-4\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-1796-title\" data-sectionid=\"4\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1796-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-4\">22 October - 28 October</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"><li class=\"activity label modtype_label \" id=\"module-73168\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/hhi-wstr-yft\">Link za pristup </a></p></div></div></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-73169\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/xqi-mcry-jsv\">Link za pristup </a></p></div></div></div></div></div></div></li></ul></div></li><li id=\"section-5\" class=\"section main clearfix current\" role=\"region\" aria-labelledby=\"sectionid-1797-title\" data-sectionid=\"5\" data-sectionreturnid=\"0\"><div class=\"left side\"><span class=\"accesshide \">Ova sedmica</span></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1797-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-5\">29 October - 4 November</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
            <span class=\"badge badge-info\">Nije dostupno</span>
        </div></div><div class=\"summary\"></div></div></li><li id=\"section-6\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-1798-title\" data-sectionid=\"6\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1798-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-6\">5 November - 11 November</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
            <span class=\"badge badge-info\">Nije dostupno</span>
        </div></div><div class=\"summary\"></div></div></li><li id=\"section-7\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-1799-title\" data-sectionid=\"7\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1799-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-7\">12 November - 18 November</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
            <span class=\"badge badge-info\">Nije dostupno</span>
        </div></div><div class=\"summary\"></div></div></li><li id=\"section-8\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-1800-title\" data-sectionid=\"8\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1800-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-8\">19 November - 25 November</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
            <span class=\"badge badge-info\">Nije dostupno</span>
        </div></div><div class=\"summary\"></div></div></li><li id=\"section-9\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-1801-title\" data-sectionid=\"9\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1801-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-9\">26 November - 2 December</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
            <span class=\"badge badge-info\">Nije dostupno</span>
        </div></div><div class=\"summary\"></div></div></li><li id=\"section-10\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-1802-title\" data-sectionid=\"10\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1802-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-10\">3 December - 9 December</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
            <span class=\"badge badge-info\">Nije dostupno</span>
        </div></div><div class=\"summary\"></div></div></li><li id=\"section-11\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-3824-title\" data-sectionid=\"11\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3824-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-11\">10 December - 16 December</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
            <span class=\"badge badge-info\">Nije dostupno</span>
        </div></div><div class=\"summary\"></div></div></li><li id=\"section-12\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-3825-title\" data-sectionid=\"12\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3825-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-12\">17 December - 23 December</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
            <span class=\"badge badge-info\">Nije dostupno</span>
        </div></div><div class=\"summary\"></div></div></li><li id=\"section-13\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-3826-title\" data-sectionid=\"13\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3826-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-13\">24 December - 30 December</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
            <span class=\"badge badge-info\">Nije dostupno</span>
        </div></div><div class=\"summary\"></div></div></li><li id=\"section-14\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-3827-title\" data-sectionid=\"14\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3827-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-14\">31 December - 6 January</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
            <span class=\"badge badge-info\">Nije dostupno</span>
        </div></div><div class=\"summary\"></div></div></li><li id=\"section-15\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-3828-title\" data-sectionid=\"15\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3828-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-15\">7 January - 13 January</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"></ul></div></li><li id=\"section-16\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-3834-title\" data-sectionid=\"16\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3834-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-16\">14 January - 20 January</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
            <span class=\"badge badge-info\">Nije dostupno</span>
        </div></div><div class=\"summary\"></div></div></li></ul></div>`;
      
        assert.equal(GoogleMeet.dajZadnjePredavanje(stranica),null);

  });

  it('Nema nijedne vjezbe', function(){

  let stranica= `<div class=\"course-content\"><h2 class=\"accesshide\">Uvodna sekcija (sedmični format)</h2><ul class=\"weeks\"><li id=\"section-0\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-1566-title\" data-sectionid=\"0\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1566-title\" class=\"sectionname accesshide\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-0\">Opšte</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"><li class=\"activity label modtype_label \" id=\"module-72184\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><strong></strong><strong>Studenti-ponovci</strong><strong>&nbsp;</strong><span><strong>trebaju do 25.10.2020. godine</strong>&nbsp;</span>javiti (mailom) <strong>predmetnom nastavniku i asistentu</strong> proslogodisnje osvojene bodove koje zele prenijeti u tekucu akademsku godinu! Student u mailu treba <strong>obavezno navesti</strong><strong> sve podatke</strong> koji se odnose na <strong>naziv predmeta i iznos bodova za svaku aktivnost</strong>&nbsp;(iz kojih zele prenijeti bodove).<br></p></div></div></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72185\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><strong>Obavještavaju se ponovci da prenos bodova mogu uraditi samo ako je u skladu sa \"Prelazne odredbe o priznavanju poena stečenih u ranijim akademskim godinama\" - nalazi se na stranici fakulteta i Zakonom o visokom obrazovanju. Bodovi za rad na vježbama i kvizovima i bodovi na parcijalnim ispitima se mogu prenijeti samo iz prethodne akademske godine.</strong><br></p></div></div></div></div></div></div></li><li class=\"activity choice modtype_choice \" id=\"module-72276\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"\" href=\"https://c2.etf.unsa.ba/mod/choice/view.php?id=72276\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/choice/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Odabir grupa za vježbe (dodana grupa, produžen rok)<span class=\"accesshide \"> Izbor</span></span></a></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72647\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><div style=\"margin-left: 30px;color:crimson;\" class=\"editor-indent\">
        <p dir=\"ltr\" style=\"text-align: left;\"><strong>Studenti koji imaju preklapanje sa drugim predmetima i terminom vježbe iz WT-a trebaju odabrati novi termin. Otvorena je nova grupa. I studenti iz grupa koji su već odabrali, a odgovara im neki drugi, slobodan termin, mogu promijeniti grupu. Studenti koji su prenijeli bodove za prisustvo ne trebaju birati grupu.</strong><br></p>
    </div></div></div></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72628\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\">Studenti grupe ponedjeljkom (od 17:00 sati) gdje su prijavljena samo dva studenta, trebaju doći u neku drugu grupu. Kako nema dovoljno studenata u toj grupi da bi se nastava normalno izvodila, grupa se ukida. Ukoliko bude još upisanih studenata koji nemaju grupu tada ćemo otvoriti novi termin.<br></p></div></div></div></div></div></div></li></ul></div></li><li id=\"section-1\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-1793-title\" data-sectionid=\"1\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1793-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-1\">1 October - 7 October</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"><li class=\"activity label modtype_label \" id=\"module-71968\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/uca-utbm-fsv\">Link za pristup predavanju</a></p><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/jcm-tkvb-djz\">Link za pristup predavanju</a></p><p dir=\"ltr\" style=\"text-align: left;\">Za pristup predavanju je potrebno koristiti fakultetski mail.</p></div></div></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-72083\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=72083&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=72083\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/pdf-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Uvodno predavanje<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72139\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><h4 dir=\"ltr\" style=\"text-align: left;\">Molimo vas popunite formu, najkasnije do 18.10.2020&nbsp; do 12:00 sati - <a href=\"https://forms.gle/zMyNVF1qrekmTNgx8\">link</a>. (Pristup sa fakultetskim mailom)<br></h4></div></div></div></div></div></div></li></ul></div></li><li id=\"section-2\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-1794-title\" data-sectionid=\"2\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1794-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-2\">8 October - 14 October</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"><li class=\"activity label modtype_label \" id=\"module-72355\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/pdn-pvky-zyd\">Link za pristup predavanju</a></p></div></div></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72356\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/hay-xfmm-pvp\">Link za pristup predavanju</a></p></div></div></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-53453\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=53453&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=53453\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/pdf-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">HTML<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity url modtype_url \" id=\"module-39663\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/url/view.php?id=39663&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/url/view.php?id=39663\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/url/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 1 - HTML<span class=\"accesshide \"> URL adresa</span></span></a></div></div></div></div></li><li class=\"activity url modtype_url \" id=\"module-72644\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"\" href=\"https://c2.etf.unsa.ba/mod/url/view.php?id=72644\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/url/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 1 - Google Meet link<span class=\"accesshide \"> URL adresa</span></span></a></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72702\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\">Link Meet-a za vježbu biće ažuriran par minuta prije početka vježbe<br></p></div></div></div></div></div></div></li></ul></div></li><li id=\"section-3\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-1795-title\" data-sectionid=\"3\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1795-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-3\">15 October - 21 October</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"><li class=\"activity label modtype_label \" id=\"module-72800\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/bfm-jfpz-gfw\">Link za pristup predavanju</a></p></div></div></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-72799\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/vjp-agsh-cgw\">Link za pristup predavanju</a></p></div></div></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-72831\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=72831&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=72831\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/pdf-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">HTML5<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-58954\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=58954&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=58954\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/pdf-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">CSS<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity url modtype_url \" id=\"module-39779\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/url/view.php?id=39779&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/url/view.php?id=39779\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/url/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 2 - CSS i layout stranice<span class=\"accesshide \"> URL adresa</span></span></a></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-47752\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=47752&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=47752\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/pdf-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 2 - Responzivni web dizajn<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity url modtype_url \" id=\"module-59102\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"\" href=\"https://c2.etf.unsa.ba/mod/url/view.php?id=59102\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/url/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 2 - Uputstvo za testiranje HTML i CSS layouta<span class=\"accesshide \"> URL adresa</span></span></a></div></div></div></div></li><li class=\"activity resource modtype_resource \" id=\"module-47843\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"window.open('https://c2.etf.unsa.ba/mod/resource/view.php?id=47843&amp;redirect=1', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); return false;\" href=\"https://c2.etf.unsa.ba/mod/resource/view.php?id=47843\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/f/archive-24\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Primjer - Grid layout<span class=\"accesshide \"> Datoteka</span></span></a></div></div></div></div></li><li class=\"activity quiz modtype_quiz \" id=\"module-53753\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"\" href=\"https://c2.etf.unsa.ba/mod/quiz/view.php?id=53753\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/quiz/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Kviz 1</span></a></div></div></div></div></li><li class=\"activity url modtype_url \" id=\"module-73035\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"activityinstance\"><a class=\"aalink\" onclick=\"\" href=\"https://c2.etf.unsa.ba/mod/url/view.php?id=73035\"><img src=\"https://c2.etf.unsa.ba/theme/image.php/boost/url/1602492614/icon\" class=\"iconlarge activityicon\" alt=\"\" role=\"presentation\" aria-hidden=\"true\"><span class=\"instancename\">Vježba 2 - Google Meet link<span class=\"accesshide \"> URL adresa</span></span></a></div></div></div></div></li></ul></div></li><li id=\"section-4\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-1796-title\" data-sectionid=\"4\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1796-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-4\">22 October - 28 October</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"><li class=\"activity label modtype_label \" id=\"module-73168\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/hhi-wstr-yft\">Link za pristup predavanju</a></p></div></div></div></div></div></div></li><li class=\"activity label modtype_label \" id=\"module-73169\"><div><div class=\"mod-indent-outer w-100\"><div class=\"mod-indent\"></div><div><div class=\"contentwithoutlink \"><div class=\"no-overflow\"><div class=\"no-overflow\"><p dir=\"ltr\" style=\"text-align: left;\"><a href=\"http://meet.google.com/xqi-mcry-jsv\">Link za pristup predavanju</a></p></div></div></div></div></div></div></li></ul></div></li><li id=\"section-5\" class=\"section main clearfix current\" role=\"region\" aria-labelledby=\"sectionid-1797-title\" data-sectionid=\"5\" data-sectionreturnid=\"0\"><div class=\"left side\"><span class=\"accesshide \">Ova sedmica</span></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1797-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-5\">29 October - 4 November</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
        <span class=\"badge badge-info\">Nije dostupno</span>
    </div></div><div class=\"summary\"></div></div></li><li id=\"section-6\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-1798-title\" data-sectionid=\"6\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1798-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-6\">5 November - 11 November</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
        <span class=\"badge badge-info\">Nije dostupno</span>
    </div></div><div class=\"summary\"></div></div></li><li id=\"section-7\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-1799-title\" data-sectionid=\"7\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1799-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-7\">12 November - 18 November</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
        <span class=\"badge badge-info\">Nije dostupno</span>
    </div></div><div class=\"summary\"></div></div></li><li id=\"section-8\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-1800-title\" data-sectionid=\"8\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1800-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-8\">19 November - 25 November</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
        <span class=\"badge badge-info\">Nije dostupno</span>
    </div></div><div class=\"summary\"></div></div></li><li id=\"section-9\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-1801-title\" data-sectionid=\"9\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1801-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-9\">26 November - 2 December</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
        <span class=\"badge badge-info\">Nije dostupno</span>
    </div></div><div class=\"summary\"></div></div></li><li id=\"section-10\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-1802-title\" data-sectionid=\"10\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-1802-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-10\">3 December - 9 December</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
        <span class=\"badge badge-info\">Nije dostupno</span>
    </div></div><div class=\"summary\"></div></div></li><li id=\"section-11\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-3824-title\" data-sectionid=\"11\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3824-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-11\">10 December - 16 December</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
        <span class=\"badge badge-info\">Nije dostupno</span>
    </div></div><div class=\"summary\"></div></div></li><li id=\"section-12\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-3825-title\" data-sectionid=\"12\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3825-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-12\">17 December - 23 December</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
        <span class=\"badge badge-info\">Nije dostupno</span>
    </div></div><div class=\"summary\"></div></div></li><li id=\"section-13\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-3826-title\" data-sectionid=\"13\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3826-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-13\">24 December - 30 December</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
        <span class=\"badge badge-info\">Nije dostupno</span>
    </div></div><div class=\"summary\"></div></div></li><li id=\"section-14\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-3827-title\" data-sectionid=\"14\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3827-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-14\">31 December - 6 January</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
        <span class=\"badge badge-info\">Nije dostupno</span>
    </div></div><div class=\"summary\"></div></div></li><li id=\"section-15\" class=\"section main clearfix\" role=\"region\" aria-labelledby=\"sectionid-3828-title\" data-sectionid=\"15\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3828-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-15\">7 January - 13 January</a></span></h3><div class=\"section_availability\"></div><div class=\"summary\"></div><ul class=\"section img-text\"></ul></div></li><li id=\"section-16\" class=\"section main clearfix hidden\" role=\"region\" aria-labelledby=\"sectionid-3834-title\" data-sectionid=\"16\" data-sectionreturnid=\"0\"><div class=\"left side\"></div><div class=\"right side\"><img class=\"icon spacer\" alt=\"\" aria-hidden=\"true\" src=\"https://c2.etf.unsa.ba/theme/image.php/boost/core/1602492614/spacer\" width=\"1\" height=\"1\"></div><div class=\"content\"><h3 id=\"sectionid-3834-title\" class=\"sectionname\"><span><a href=\"https://c2.etf.unsa.ba/course/view.php?id=119#section-16\">14 January - 20 January</a></span></h3><div class=\"section_availability\"><div class=\"availabilityinfo ishidden\">
        <span class=\"badge badge-info\">Nije dostupno</span>
    </div></div><div class=\"summary\"></div></div></li></ul></div>`

    assert.equal(GoogleMeet.dajZadnjuVježbu(stranica),null);
 


  });

  it('Prazan string vraća se null', function(){

    assert.equal(GoogleMeet.dajZadnjuVježbu(""),null);
    assert.equal(GoogleMeet.dajZadnjePredavanje(""),null);
  });


  });

  describe('#OstaliTestniSlučajevi', function(){


 
    it('Vježbe u svakoj drugoj sedmici', function(){
  
        let stranica= `<!DOCTYPE html>
        <html lang="en">
        <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Test GoogleMeeta</title>
        </head>
        <body>
        
        <h2>Test GoogleMeeta</h2>
        <div class="course-content">

        <ul class="weeks">
 
        <li id="section-0"><a href="https://meet.google.com/bnd-jink-olr"> Pristup prvoj vježbi </a></li>
        <li id="section-1"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-2"><a href="https://meet.google.com/har-tsfg-adg"> Pristup drugoj vježbi. </a></li>
        <li id="section-3"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-4"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Pristup trećoj vježbi. </a></li>
        <li id="section-5"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-6"><a href="https://meet.google.com/dan-test-zrj"> Pristup četvrtoj vježbi. </a></li>
        <li id="section-7"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-8"><a href="https://meet.google.com/nak-ydhu-doh"> Pristup petoj vježbi. </a></li>
        <li id="section-9"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-10"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Pristup šestoj vježbi. </a></li>
        <li id="section-11"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-12"><a href="https://meet.google.com/ksn-adyu-adh"> Pristup sedmoj vježbi. </a></li>
        <li id="section-13"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-14"><a href="https://meet.google.com/kan-yfau-4oh"> Pristup osmoj vježbi. </a></li>
        <li id="section-15"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-16"><a href="https://meet.google.com/hdf-ozie-lok"> Pristup devetoj vježbi. </a></li>


        </ul>
        </div>
        
        </body>
        </html>
        `


    assert.equal('https://meet.google.com/hdf-ozie-lok', GoogleMeet.dajZadnjuVježbu(stranica)); //pisem sopstveni html kod jer mi se vise isplati



    });


    it('Postoje vježbe, ali pogrešan link ( met umjesto meet )', function(){
  
        let stranica= `<!DOCTYPE html>
        <html lang="en">
        <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Test GoogleMeeta</title>
        </head>
        <body>
        
        <h2>Test GoogleMeeta</h2>
        <div class="course-content">

        <ul class="weeks">
 
        <li id="section-0"><a href="https://met.google.com/bnd-jink-olr"> Pristup prvoj vježbi </a></li>
        <li id="section-1"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-2"><a href="https://met.google.com/har-tsfg-adg"> Pristup drugoj vježbi. </a></li>
        <li id="section-3"><a href="https://met.google.com/gse-lskt-hat"> Ove sedmice nema vježbi. </a></li>
        <li id="section-4"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Pristup trećoj vježbi. </a></li>
        <li id="section-5"><a href="https://met.google.com/noc-zest-tes"> Ove sedmice nema vježbi. </a></li>
        <li id="section-6"><a href="https://met.google.com/dan-test-zrj"> Pristup četvrtoj vježbi. </a></li>
        <li id="section-7"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-8"><a href="https://met.google.com/nak-ydhu-doh"> Pristup petoj vježbi. </a></li>
        <li id="section-9"><a href="https://met.google.com/npk-ylyu-ash"> Ove sedmice nema vježbi. </a></li>
        <li id="section-10"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Pristup šestoj vježbi. </a></li>
        <li id="section-11"><a href="https://met.google.com/aan-rdzu-agh"> Ove sedmice nema vježbi. </a></li>
        <li id="section-12"><a href="https://met.google.com/ksn-adyu-adh"> Pristup sedmoj vježbi. </a></li>
        <li id="section-13"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-14"><a href="https://met.google.com/kan-yfau-4oh"> Pristup osmoj vježbi. </a></li>
        <li id="section-15"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
        <li id="section-16"><a href="https://met.google.com/hdf-ozie-lok"> Pristup devetoj vježbi. </a></li>


        </ul>
        </div>
        
        </body>
        </html>
        `


    assert.equal(null, GoogleMeet.dajZadnjuVježbu(stranica));


  });

  it('Test kada postoje linkovi sa ispravnim url-om ali ne sadrže tekst ‘vjezb’,’vježb’ i ’predavanj’ (null)', function(){
  
    let stranica= `<!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test GoogleMeeta</title>
    </head>
    <body>
    
    <h2>Test GoogleMeeta</h2>
    <div class="course-content">

    <ul class="weeks">

    <li id="section-0"><a href="https://meet.google.com/bnd-jink-olr"> Pristup prvoj vji </a></li>
    <li id="section-1"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vjei. </a></li>
    <li id="section-2"><a href="https://meet.google.com/har-tsfg-adg"> Pristup drugoj vjei. </a></li>
    <li id="section-3"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vjebi. </a></li>
    <li id="section-4"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Pristup trećoj vjbi. </a></li>
    <li id="section-5"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vjebi. </a></li>
    <li id="section-6"><a href="https://meet.google.com/dan-test-zrj"> Pristup četvrtoj vjebi. </a></li>
    <li id="section-7"><a href="https://meet.google.com/ned-trwt-mnt"> Link za predvanje ove sedmice. </a></li>
    <li id="section-8"><a href="https://meet.google.com/nak-ydhu-doh"> Pristup petoj vjebi. </a></li>
    <li id="section-9"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vjebi. </a></li>
    <li id="section-10"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Pristup šestoj vjebi. </a></li>
    <li id="section-11"><a href="https://meet.google.com/otd-ntt-nza"> Link za predvanje ove sedmice. </a></li>
    <li id="section-12"><a href="https://meet.google.com/ksn-adyu-adh"> Pristup sedmoj vjei. </a></li>
    <li id="section-13"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vjebi. </a></li>
    <li id="section-14"><a href="https://meet.google.com/kan-yfau-4oh"> Pristup osmoj vjei. </a></li>
    <li id="section-15"><a href="https://meet.google.com/bab-nest-kik"> Link za predavnje ove sedmice. </a></li>
    <li id="section-16"><a href="https://meet.google.com/hdf-ozie-lok"> Pristup devetoj vjbi. </a></li>


    </ul>
    </div>
    
    </body>
    </html>
    `


assert.equal(null, GoogleMeet.dajZadnjuVježbu(stranica));
assert.equal(null, GoogleMeet.dajZadnjePredavanje(stranica));


});



it('Linkovi van liste, jedini koji je ok unutar li i a tagova je posljednji i on je tačan rezulat', function(){
  
    let stranica= `<!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test GoogleMeeta</title>
    </head>
    <body>
    
    <h2>Test GoogleMeeta</h2>
    <div class="course-content">
    <a href="https://meet.google.com/bnd-jldk-edr"> Pristup prvoj vježbi </a>
    <a href="https://meet.google.com/bsd-kinn-hlr"> Pristup drugoj vježbi </a>
    <a href="https://meet.google.com/ffd-asdk-itl"> Pristup trećoj vježbi </a>
    <a href="https://meet.google.com/dan-test-zrj"> Pristup četvrtoj vježbi. </a>
    <a href="https://meet.google.com/nak-ydhu-doh"> Pristup petoj vježbi. </a>
    <a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Pristup šestoj vježbi. </a>
    <a href="https://meet.google.com/ksn-adyu-adh"> Pristup sedmoj vježbi. </a>ž
    <a href="https://meet.google.com/kan-yfau-4oh"> Pristup osmoj vježbi. </a>
    
    <ul class="weeks">

    <li id="section-0"></li>
    <li id="section-13"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-15"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-16"><a href="https://meet.google.com/hdf-ozie-lok"> Pristup devetoj vježbi. </a></li>


    </ul>
    </div>
    
    </body>
    </html>
    `


assert.equal('https://meet.google.com/hdf-ozie-lok', GoogleMeet.dajZadnjuVježbu(stranica)); //pisem sopstveni html kod jer mi se vise isplati

});

it('Test kada su predavanja i vježbe samo u prvoj sedmici (rezultat url vježbe iz prve sedmice i url predavanja iz prve sedmice',function(){
  
    let stranica= `<!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test GoogleMeeta</title>
    </head>
    <body>
    
    <h2>Test GoogleMeeta</h2>
    <div class="course-content">

    <ul class="weeks">

    <li id="section-0"><a href="https://meet.google.com/bnd-jink-olr"> Pristup prvoj vježbi </a></li>
    <li id="section-1"><a href="https://meet.google.com/wad-dsfe-ttt"> Link za predavanje ove sedmice. </a></li>
    <li id="section-2"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-3"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-4"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-5"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-6"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-7"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-8"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-9"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-10"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-11"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-12"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-13"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-14"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-15"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="section-16"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>


    </ul>
    </div>
    
    </body>
    </html>
    `


assert.equal('https://meet.google.com/bnd-jink-olr', GoogleMeet.dajZadnjuVježbu(stranica));
assert.equal('https://meet.google.com/wad-dsfe-ttt', GoogleMeet.dajZadnjePredavanje(stranica));

});

it('Test kada nije validan html kod - kada ne sadrži div ‘course-content’ sa ul listom ‘weeks’ i elementima liste ‘section-*’.',function(){
  
    let stranica= `<!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test GoogleMeeta</title>
    </head>
    <body>
    
    <h2>Test GoogleMeeta</h2>
    <div class="Promašaj">

    <ul class="days">

    <li id="fail-0"><a href="https://meet.google.com/bnd-jink-olr"> Pristup prvoj vježbi </a></li>
    <li id="fail-1"><a href="https://meet.google.com/wad-dsfe-ttt"> Link za predavanje ove sedmice. </a></li>
    <li id="fail-2"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-3"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-4"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-5"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-6"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-7"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-8"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-9"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-10"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-11"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-12"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-13"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-14"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-15"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>
    <li id="fail-16"><a href="https://c2.etf.unsa.ba/mod/forum/view.php?id=72038"> Ove sedmice nema vježbi. </a></li>


    </ul>
    </div>
    
    </body>
    </html>
    `


assert.equal(null, GoogleMeet.dajZadnjuVježbu(stranica));
assert.equal(null, GoogleMeet.dajZadnjePredavanje(stranica));

});


});

});