const express = require('express')
const app = express()
var fs = require('fs');
const mysql = require('mysql2');
const path = require('path')
const Moment = require('moment');
const url = require('url');
var os = require("os");
const bodyParser = require('body-parser');
var sequelize = require('sequelize');






const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: "root",
   database: 'wt65st'
});


connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});


app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
var urlencodedParser = bodyParser.urlencoded({extended: false});


const MomentRange = require('moment-range');

const moment = MomentRange.extendMoment(Moment);


const db = require('./models');



app.use(express.static(__dirname));
app.use(express.urlencoded({extended: false}))
app.set("view engine", "ejs")
app.set("views", path.join(__dirname,"views"))
app.engine('html', require('ejs').renderFile);




app.use("/views",express.static(__dirname + "/views"));
app.use(express.static(__dirname + "/public"));

app.use(function(req, res, next) {
  if (req.headers['content-type'] === 'application/json;') {
    req.headers['content-type'] = 'application/json';
  }
  next();
});

app.get("/", (req,res)=>{
  res.render("unosRasporeda.ejs") //ucitavanje htmla
})

app.get("/saljiPredmete", (req,res)=>{
  res.sendFile(path.join(__dirname + '/unosRasporeda.html'));
})


app.get("/v2", (req,res)=>{
  res.sendFile(path.join(__dirname + '/studenti.html'));
})


app.post("/v2/Student", async (req,res)=>{
   
  const studentDetails =await db.Student.build({
    ime: req.body.ime,
    index: req.body.indeks,
});
await studentDetails.save()
if(!studentDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nije se kreirao novi student'
});
}
res.status(200).send({
  status: 200,
  message: 'Novi student kreiran'
});
  
})

app.post("/v2/StudentZ2", async (req,res)=>{
  
  let studentIdizGrupe;
  let idPredmeta;
  let praviNazivPredmeta;
  let nazivGrupeBaza;

  let imaSmislaTrazit=false;
  let nazivUnesenogPredmeta=req.body.selekt.toString().substr(6);
  // console.log(nazivUnesenogPredmeta); //treba mi naziv predmeta, jer cu iako postoji vec student ipak ga unijeti updateovanog sa novom grupom ako ima DRUGU grupu iz ISTOG predmeta
   var errori = [];
   let nazivUneseneGrupe=req.body.selekt.toString().substr(0,6);
   const studentskiPodaci= await db.Student.findAll();
 
  // console.log(studentskiPodaci[0].indeks); //na ovaj nacin cu uzet sta je trenutno u bazi i provjeravat ima li poklapanja ...
  
   let structRaspored=new Array();
   let niz=req.body.tekstarea.toString().split("\n");
     for (let i=0;i<niz.length;i++){
    
      let info=niz[i].split(",");
      structRaspored[i] = {ime : info[0], indeks : info[1]};
      
     }
    ;
    //gdje je selekt razlicit od 
   for (let j=0;j<studentskiPodaci.length;j++){  //provjera identicnog studenta
     for (let it=0;it<structRaspored.length;it++){
    /*   studentIdizGrupe=await db.Student.findAll({ //uzimam id studenta kojeg dodajem a vec ima al u novu grupu ga dodajem 
         limit: 1,
         attributes: ['id'],
         order: [ [ 'createdAt', 'DESC' ]],
         where: {
           indeks: structRaspored[it].indeks
         }
       });
       if (studentIdizGrupe.length>0){
         imaSmislaTrazit=true;
       idPredmeta=await db.Grupa.findAll({
         limit:1,
         attributes: ['PredmetId'],
         where:{
           id:studentIdizGrupe[0].id
         }
       })
    
      //console.log(idPredmeta[0].PredmetId);
      praviNazivPredmeta=await db.Predmet.findAll({
       limit:1,
       attributes: ['naziv'],
       where:{
         id:idPredmeta[0].PredmetId
       }
      })

     

      nazivGrupeBaza=await db.Grupa.findAll({
        limit:1,
        attributes:['naziv'],
        where:{
          id:studentIdizGrupe[0].id
        }
      })

      nazivGrupeBaza[0].naziv=nazivGrupeBaza[0].naziv.substr(0,6);

    }
     // console.log(praviNazivPredmeta[0].naziv); eh ovdje sad imam naziv predmeta dobar
    
 */

for (let j=0;j<studentskiPodaci.length;j++){  //provjera studenta sa istim indeksom ali drugim imenom
  for (let it=0;it<structRaspored.length;it++){
    if (studentskiPodaci[j].ime!=structRaspored[it].ime && studentskiPodaci[j].indeks==structRaspored[it].indeks){
      errori.push('Student '+structRaspored[it].ime+' nije kreiran jer postoji student '+studentskiPodaci[j].ime+' sa istim indeksom '+structRaspored[it].indeks);
      structRaspored[it]=structRaspored[it+1];
      structRaspored.length--;
+        it--;
    }
  }
}
    
for (let j=0;j<studentskiPodaci.length;j++){  //provjera identicnog studenta
  for (let it=0;it<structRaspored.length;it++){
    if (studentskiPodaci[j].ime==structRaspored[it].ime && studentskiPodaci[j].indeks==structRaspored[it].indeks){
      structRaspored[it]=structRaspored[it+1];
      structRaspored.length--;
+        it--;
    }
  }
}
   
    
  
     
   
    

  }
}
   
   
   for (let k=0;k<structRaspored.length;k++){
     const studentDetails =await db.Student.build({
       ime: structRaspored[k].ime,
       indeks: structRaspored[k].indeks,
   });
  
   let nazivPredmeta="";
   if (req.body.selekt.toString().includes("BWT")){
     nazivPredmeta+="BWT";
   }
   else if (req.body.selekt.toString().includes("ASP")){
     nazivPredmeta+="ASP"
   }
   else if (req.body.selekt.toString().includes("PJP")){
     nazivPredmeta+="PJP"
   }
   else {
     nazivPredmeta+="FWT"
   }
   const oPredmetu=await db.Predmet.build({
     naziv:nazivPredmeta,
   })
   
   
   await studentDetails.save()
   await oPredmetu.save()
   
   let trazeniId= await db.Predmet.findAll({ //uzimam zadnji id grupe koji je tek dodan
     limit: 1,
     attributes: ['id'],
     order: [ [ 'createdAt', 'DESC' ]]
   });

 
   const grupaDetails =await db.Grupa.build({
     naziv: req.body.selekt,
     PredmetId: trazeniId[0].id,
   });
 
  
   await grupaDetails.save()
   

   
 
   }
 
 
 
 
 
  
     //uzet cu zadnji iz grupa, zadnje ubacene iz studenata i ubacit ih u studentskeGrupe i tako ce biti povezani M:N vezom.
 
   db.Grupa.findAll({ //uzimam zadnji id grupe koji je tek dodan
   limit: 1,
   attributes: ['id'],
   order: [ [ 'createdAt', 'DESC' ]]
 }).then(function(entries){
   db.Student.findAll({
     limit: structRaspored.length, //uzimam onoliko koliko sam i upisao zadnji put studenata jer svakako svi idu u istu grupu
     attributes: ['id'],
     order: [ [ 'createdAt', 'DESC' ]]
 
   }).then(function(entries2){
      //ovdje cu upisat u bazu
      let brojac=0;
      let brojac2=0;
   
      for (let put=0;put<structRaspored.length;put++){ //insertovat ce se onoliko puta koliko ima ovih novih studenata...
      connection.query("INSERT INTO studentskegrupe(GrupaId,StudentId) VALUES ("+entries[0].id+","+entries2[put].id+");", function (err, result, fields) {
       if (err) throw err; //morao sam ovdje koristit stari sql jer studentskegrupe drzim samo na phpmyadminu tj kreiraju se same pri pokretanju mainjs fajla, da sam htio
       //koristit create i cisti sequelize onda bih morao napravit model studentskegrupe a lakse mi je i prakticnije ovako samo jedan sql upit napisati
     });  
    }
   })
 }); 
 

 
 res.status(200).send(errori);
  
})


app.post("/v2/Aktivnost", async (req,res)=>{
    
  const AktivnostDetails =await db.Aktivnost.build({
    naziv: req.body.naziv,
    pocetak: req.body.pocetak,
    kraj: req.body.kraj,
});
await AktivnostDetails.save()
if(!AktivnostDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nije se kreirala nova aktivnost'
});
}
res.status(200).send({
  status: 200,
  message: 'Nova aktivnost kreirana'
});
  
})


app.post("/v2/Dan", async (req,res)=>{
    
  const danDetails =await db.Dan.build({
    ime: req.body.naziv,
});
await danDetails.save()
if(!danDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nije se kreirao novi dan'
});
}
res.status(200).send({
  status: 200,
  message: 'Novi dan kreiran'
});
  
})


app.post("/v2/Grupa", async (req,res)=>{
    
  const grupaDetails =await db.Grupa.build({
    naziv: req.body.naziv,
});
await grupaDetails.save()
if(!grupaDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nije se kreirala nova grupa'
});
}
res.status(200).send({
  status: 200,
  message: 'Nova grupa kreirana'
});
  
})



app.post("/v2/Predmet", async (req,res)=>{
    
  const predmetDetails =await db.Predmet.build({
    naziv: req.body.naziv,
});
await predmetDetails.save()
if(!predmetDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nije se kreirao novi predmet'
});
}
res.status(200).send({
  status: 200,
  message: 'Novi predmet kreiran'
});
  
})

app.post("/v2/PredmetZ3", async (req,res)=>{
    
  const predmetDetails =await db.Predmet.build({
    naziv: req.body.ime,
});

const aktivnostdetalji =await db.Aktivnost.build({
  naziv: req.body.aktiv,
});


await predmetDetails.save()
await aktivnostdetalji.save()
if(!predmetDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nije se kreirao novi predmet'
});
}
res.status(200).send({
  status: 200,
  message: 'Novi predmet kreiran'
});
  
})

app.post("/v2/Tip", async (req,res)=>{
    
  const tipDetails =await db.Tip.build({
    naziv: req.body.naziv,
});
await tipDetails.save()
if(!tipDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nije se kreirao novi tip'
});
}
res.status(200).send({
  status: 200,
  message: 'Novi tip kreiran'
});
  
})


app.get("/v2/Student", async(req,res)=>{

  const StudentDetails = await db.Student.findAll();
  if(!StudentDetails){
     return res.status(200).send({
     status: 404,
     message: 'Nema podataka za dohvatiti'
  });
  }
  res.status(200).send({
     status: 200,
     message: 'Podaci pronadjeni uspjesno u bazi'
  });
})


app.get("/v2/Aktivnost", async(req,res)=>{

  const aktivnostDetails = await db.Aktivnost.findAll();
  if(!aktivnostDetails){
     return res.status(200).send({
     status: 404,
     message: 'Nema podataka za dohvatiti'
  });
  }
  res.status(200).send({
     status: 200,
     message: 'Podaci pronadjeni uspjesno u bazi'
  });
})


app.get("/v2/Tip", async(req,res)=>{

  const tipDetails = await db.Tip.findAll();
  if(!tipDetails){
     return res.status(200).send({
     status: 404,
     message: 'Nema podataka za dohvatiti'
  });
  }
  res.status(200).send({
     status: 200,
     message: 'Podaci pronadjeni uspjesno u bazi'
  });
})


app.get("/v2/Dan", async(req,res)=>{

  const danDetails = await db.Dan.findAll();
  if(!danDetails){
     return res.status(200).send({
     status: 404,
     message: 'Nema podataka za dohvatiti'
  });
  }
  res.status(200).send({
     status: 200,
     message: 'Podaci pronadjeni uspjesno u bazi'
  });
})

app.get("/v2/Grupa", async(req,res)=>{

  const grupaDetails = await db.Grupa.findAll();
  if(!grupaDetails){
     return res.status(200).send({
     status: 404,
     message: 'Nema podataka za dohvatiti'
  });
  }
  res.status(200).send({
     status: 200,
     message: 'Podaci pronadjeni uspjesno u bazi'
  });
})


app.get("/v2/Predmet", async(req,res)=>{

  const predmetDetails = await db.Predmet.findAll();
  if(!predmetDetails){
     return res.status(200).send({
     status: 404,
     message: 'Nema podataka za dohvatiti'
  });
  }
  res.status(200).send({
     status: 200,
     message: 'Podaci pronadjeni uspjesno u bazi'
  });
})


app.put("/v2/Student", async(req,res)=>{

  const id=req.body.id;
  const studentDetails =await db.Student.update({
    ime: req.body.name,
    indeks: req.body.indeks,
},
{where: {id: req.body.id} });
if(!studentDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nisu pronadjeni podaci za update'
});
}
res.status(200).send({
  status: 200,
  message: 'Podaci updateovani uspjesno'
});


})

app.put("/v2/Aktivnost", async(req,res)=>{

  const id=req.body.id;
  const aktivnostDetails =await db.Aktivnost.update({
    naziv: req.body.naziv,
    pocetak: req.body.pocetak,
    kraj: req.body.kraj,
},
{where: {id: req.body.id} });
if(!aktivnostDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nisu pronadjeni podaci za update'
});
}
res.status(200).send({
  status: 200,
  message: 'Podaci updateovani uspjesno'
});

})


app.put("/v2/Dan", async(req,res)=>{

  const id=req.body.id;
  const danDetails =await db.Dan.update({
    naziv: req.body.naziv,
},
{where: {id: req.body.id} });
if(!danDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nisu pronadjeni podaci za update'
});
}
res.status(200).send({
  status: 200,
  message: 'Podaci updateovani uspjesno'
});

})


app.put("/v2/Grupa", async(req,res)=>{

  const id=req.body.id;
  const grupaDetails =await db.Grupa.update({
    naziv: req.body.naziv,
},
{where: {id: req.body.id} });
if(!grupaDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nisu pronadjeni podaci za update'
});
}
res.status(200).send({
  status: 200,
  message: 'Podaci updateovani uspjesno'
});

})


app.put("/v2/Predmet", async(req,res)=>{

  const id=req.body.id;
  const predmetDetails =await db.Predmet.update({
    naziv: req.body.naziv,
},
{where: {id: req.body.id} });
if(!predmetDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nisu pronadjeni podaci za update'
});
}
res.status(200).send({
  status: 200,
  message: 'Podaci updateovani uspjesno'
});

})

app.put("/v2/Tip", async(req,res)=>{

  const id=req.body.id;
  const tipDetails =await db.Tip.update({
    naziv: req.body.naziv,
},
{where: {id: req.body.id} });
if(!tipDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nisu pronadjeni podaci za update'
});
}
res.status(200).send({
  status: 200,
  message: 'Podaci updateovani uspjesno'
});

})

app.delete("/v2/Student", async(req,res)=>{
  
  const id= req.body.id;
  const studentDetails = await db.Student.destroy({
  where: { id: id }
});
if(!studentDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nema se sta obrisati'
});
}
res.status(200).send({
  status: 200,
  message: 'Obrisani podaci uspjesno'
});
})

app.delete("/v2/Tip", async(req,res)=>{
  
  const id= req.body.id;
  const tipDetails = await db.Tip.destroy({
  where: { id: id }
});
if(!tipDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nema se sta obrisati'
});
}
res.status(200).send({
  status: 200,
  message: 'Obrisani podaci uspjesno'
});
})

app.delete("/v2/Predmet", async(req,res)=>{
  
  const id= req.body.id;
  const predmetDetails = await db.Predmet.destroy({
  where: { id: id }
});
if(!predmetDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nema se sta obrisati'
});
}
res.status(200).send({
  status: 200,
  message: 'Obrisani podaci uspjesno'
});
})

app.delete("/v2/PredmetZ3", async(req,res)=>{
  
  const id= req.body.ime;
  const predmetDetails = await db.Predmet.destroy({
  where: { naziv: id }
});
 const id2=req.body.aktiv;
const deleteAktiv = await db.Aktivnost.destroy({
  where: { naziv: id2 }
});
if(!predmetDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nema se sta obrisati'
});
}
res.status(200).send({
  status: 200,
  message: 'Obrisani podaci uspjesno'
});
})

app.delete("/v2/Grupa", async(req,res)=>{
  
  const id= req.body.id;
  const grupaDetails = await db.Grupa.destroy({
  where: { id: id }
});
if(!grupaDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nema se sta obrisati'
});
}
res.status(200).send({
  status: 200,
  message: 'Obrisani podaci uspjesno'
});
})

app.delete("/v2/Dan", async(req,res)=>{
  
  const id= req.body.id;
  const danDetails = await db.Dan.destroy({
  where: { id: id }
});
if(!danDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nema se sta obrisati'
});
}
res.status(200).send({
  status: 200,
  message: 'Obrisani podaci uspjesno'
});
})


app.delete("/v2/Aktivnost", async(req,res)=>{
  
  const id= req.body.id;
  const aktivnostDetails = await db.Aktivnost.destroy({
  where: { id: id }
});
if(!aktivnostDetails){
  return res.status(200).send({
    status: 404,
    message: 'Nema se sta obrisati'
});
}
res.status(200).send({
  status: 200,
  message: 'Obrisani podaci uspjesno'
});
})







app.post("/raspored", (req,res)=>{
  let structRaspored=new Array();
  let mozeSeUpisati=true;
  k=["PONEDJELJAK", "UTORAK", "SRIJEDA", "CETVRTAK", "ČETVRTAK", "PETAK"]; //niz odgovarajucih stringova, pa onda uporedjujem to sa upperCaseom unosa u formi, da se prihvati i npr uToRaK
  if (req.body.formNaziv.length!=0 && req.body.formAktivnost.length!=0 && req.body.formDan.length!=0 && (
    req.body.formDan.toUpperCase()==k[0] || req.body.formDan.toUpperCase()==k[1] || req.body.formDan.toUpperCase()==k[2] || req.body.formDan.toUpperCase()==k[3]
    || req.body.formDan.toUpperCase()==k[4] || req.body.formDan.toUpperCase()==k[5]
  )){
   fs.readFile("raspored.csv", (err,data)=>{

   if (err) throw err;
   let niz=data.toString().split("\n");
   

   for (let i=0;i<niz.length-1;i++){
  
    let info=niz[i].split(",");
    structRaspored[i] = {datum1 : info[3], datum2 : info[4], dan : info[2], aktivnost:info[1]};
  }
   
  for (let j=0;j<structRaspored.length;j++){

    let timesCSVPocetak=structRaspored[j].datum1.split(":");
    let timesCSVKraj=structRaspored[j].datum2.split(":");  //ovdje uzimam doslovno sve moguce sate i minute iz svakog ponaosob clana fajla raspored.csv ali i 
                                                           //unesenog u formi, pa uporedjujem pomocu moment.js da li se overlapaju
    let timesCSVSatiPocetak=timesCSVPocetak[0];
    let timesCSVMinutePocetak=timesCSVPocetak[1];

    let timesCSVSatiKraj=timesCSVKraj[0];
    let timesCSVMinuteKraj=timesCSVKraj[1];


    let timesFormPocetak=req.body.appt1.split(":");
    let timesFormKraj=req.body.appt2.split(":");

    let timesFormSatiPocetak=timesFormPocetak[0];
    let timesFormMinutePocetak=timesFormPocetak[1];

    let timesFormSatiKraj=timesFormKraj[0];
    let timesFormMinuteKraj=timesFormKraj[1];

    var range  = moment.range(new Date(2020, 10, 10, timesCSVSatiPocetak, timesCSVMinutePocetak), new Date(2020, 10, 10, timesCSVSatiKraj, timesCSVMinuteKraj));
    var range2 = moment.range(new Date(2020, 10, 10, timesFormSatiPocetak, timesFormMinutePocetak), new Date(2020, 10, 10, timesFormSatiKraj, timesFormMinuteKraj));
    if (structRaspored[j].dan===req.body.formDan && range.overlaps(range2))
         mozeSeUpisati=false; //kontrolni bool za upis
         if ((req.body.formAktivnost.includes("vjezbe") || req.body.formAktivnost.includes("vježbe") || req.body.formAktivnost.includes("Vjezbe") ||
         req.body.formAktivnost.includes("Vježbe") || req.body.formAktivnost.includes("vjezba") || req.body.formAktivnost.includes("vježba")  
         || req.body.formAktivnost.includes("Vjezba") || req.body.formAktivnost.includes("Vježba")) && req.body.formAktivnost!=structRaspored[j].aktivnost && 
         !(structRaspored[j].aktivnost.includes("Predavanje")))
         mozeSeUpisati=true;
   }
    
   let upisniRed=req.body.formNaziv+","+req.body.formAktivnost+","+req.body.formDan+","+req.body.appt1+","+req.body.appt2+"\n";  
  
   if (mozeSeUpisati===true){
    fs.appendFile("raspored.csv", upisniRed, (err)=>{
        if (err) res.send("Problem sa upisivanjem u datoteku.");
        res.send("Upisano u raspored.");
  
    })
  }
  else{
      res.send("Taj termin se poklapa sa drugom aktivnoscu!");
  }

})

  //moram prolaziti kroz structRaspored, ako je dan i vrijeme isto, mozeSeUpisati=false i onda ako je mozeSeUpisati=true, onda samo tad upisi...
}

})


function compareValues(key, order = 'asc') {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // u ovom slucaju zadani atribut ne postoji u tom nizu objekata tj u objektima tog niza
      return 0;
    }
   
    let varA=a[key];
    
    if (typeof a[key] === 'string'){
      varA=a[key].toUpperCase();
    }
     
  // ovdje https://morioh.com/p/9caf3015e0c0 pronadjena funkcija, a zatim modificirana tj prilagodjena zadatku 
    let varB=b[key];
    
    if (typeof b[key] === 'string'){
      varB=b[key].toUpperCase();
    }


    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
}


//saljem get na /raspored, citam datoteku, ako je nema ili se ne moze procitati, npr posaljem je u views folder, izbacit ce json za gresku. ako je ima procita sve, 
//pravi strukture, i pise ih u json objekat.
app.get("/raspored", (req,res)=>{

let structRaspored=new Array();
const urlServera = new URL('http://localhost:8080'+req.url);


const parametriServera = urlServera.searchParams;
  
  res.writeHead(200,  {'Content-Type': 'application/json'}); 
  //ovdje sam ukljucio da ima i accept header, samo ukloniti treba accept header pa ce se ispisati json od svih objekata u rasporedu, 
  //a za testiranje errora kada datoteka nije kreirana, premjestao sam je u views folder pa se ispise error u JSON-u
  fs.readFile("raspored.csv", (err,data)=>{
    
    
    if (err){
      var error={"greska":"Datoteka raspored.csv nije kreirana!"};
      res.write(JSON.stringify(error));
      res.end();
    }
    
    const dan = parametriServera.get('dan');
    if (res.hasHeader("Accept", "text/csv") && dan==null){
      res.write(data);
      res.end();
    }

    let niz=data.toString().split("\n");
     
   
    if (dan!=null){
      for (let i=0;i<niz.length-1;i++){  //ako postoji dan, tj nije null parametar, radi sve kao i inace, samo sad ispisuj ovisno o tome postoji li accept header
     
        let info=niz[i].split(",");
        structRaspored[i] = {naziv:info[0], aktivnost:info[1], dan:info[2], pocetak : info[3], kraj : info[4]};
      
      }
      const sortiranje = parametriServera.get('sort');

      if (sortiranje!=null){
        if (sortiranje.includes("D")){
         if (sortiranje.split("D")[1]==="naziv"){
           structRaspored.sort(compareValues('naziv','desc'))
         }
       }
     
       if (sortiranje.includes("D")){
         if (sortiranje.split("D")[1]==="dan"){
           structRaspored.sort(compareValues('dan','desc'))
         }
       }
     
       if (sortiranje.includes("D")){
         if (sortiranje.split("D")[1]==="aktivnost"){
           structRaspored.sort(compareValues('aktivnost','desc'))
         }
       }
     
       if (sortiranje.includes("D")){
         if (sortiranje.split("D")[1]==="pocetak"){
           structRaspored.sort(compareValues('pocetak','desc'))
         }
       }
     
       if (sortiranje.includes("D")){
         if (sortiranje.split("D")[1]==="kraj"){
           structRaspored.sort(compareValues('kraj'))
         }
       }
       if (sortiranje.includes("A")){
         if (sortiranje.split("A")[1]==="naziv"){
           structRaspored.sort(compareValues('naziv'))
         }
       }
     
       if (sortiranje.includes("A")){
         if (sortiranje.split("A")[1]==="dan"){
           structRaspored.sort(compareValues('dan'))
         }
       }
     
       if (sortiranje.includes("A")){
         if (sortiranje.split("A")[1]==="aktivnost"){
           structRaspored.sort(compareValues('aktivnost'))
         }
       }
     
       if (sortiranje.includes("A")){
         if (sortiranje.split("A")[1]==="pocetak"){
           structRaspored.sort(compareValues('pocetak'))
         }
       }
     
       if (sortiranje.includes("A")){
         if (sortiranje.split("A")[1]==="kraj"){
           structRaspored.sort(compareValues('kraj'))
         }
       }
     }

     for (let k=0;k<niz.length-1;k++){ 
      if (!(res.hasHeader("Accept", "text/csv")) && dan===structRaspored[k].dan) res.write(JSON.stringify(structRaspored[k]));
      if ((res.hasHeader("Accept", "text/csv")) && dan===structRaspored[k].dan) res.write(structRaspored[k].naziv+","+structRaspored[k].aktivnost+","+structRaspored[k].dan+","+
      structRaspored[k].pocetak+","+structRaspored[k].kraj+"\n");
     }
      res.end();
    }

    const sortiranje = parametriServera.get('sort');

    for (let i=0;i<niz.length-1;i++){
     let info=niz[i].split(",");
     structRaspored[i] = {naziv:info[0], aktivnost:info[1], dan:info[2], pocetak : info[3], kraj : info[4]};
   }
   
   if (sortiranje!=null){
   if (sortiranje.includes("D")){
    if (sortiranje.split("D")[1]==="naziv"){
      structRaspored.sort(compareValues('naziv','desc'))
    }
  }

  if (sortiranje.includes("D")){
    if (sortiranje.split("D")[1]==="dan"){
      structRaspored.sort(compareValues('dan','desc'))
    }
  }

  if (sortiranje.includes("D")){
    if (sortiranje.split("D")[1]==="aktivnost"){
      structRaspored.sort(compareValues('aktivnost','desc'))
    }
  }

  if (sortiranje.includes("D")){
    if (sortiranje.split("D")[1]==="pocetak"){
      structRaspored.sort(compareValues('pocetak','desc'))
    }
  }

  if (sortiranje.includes("D")){
    if (sortiranje.split("D")[1]==="kraj"){
      structRaspored.sort(compareValues('kraj','desc'))
    }
  }
  if (sortiranje.includes("A")){
    if (sortiranje.split("A")[1]==="naziv"){
      structRaspored.sort(compareValues('naziv'))
    }
  }

  if (sortiranje.includes("A")){
    if (sortiranje.split("A")[1]==="dan"){
      structRaspored.sort(compareValues('dan'))
    }
  }

  if (sortiranje.includes("A")){
    if (sortiranje.split("A")[1]==="aktivnost"){
      structRaspored.sort(compareValues('aktivnost'))
    }
  }

  if (sortiranje.includes("A")){
    if (sortiranje.split("A")[1]==="pocetak"){
      structRaspored.sort(compareValues('pocetak'))
    }
  }

  if (sortiranje.includes("A")){
    if (sortiranje.split("A")[1]==="kraj"){
      structRaspored.sort(compareValues('kraj'))
    }
  }
}
  
  for (let j=0;j<niz.length-1;j++){
  if (!(res.hasHeader("Accept", "text/csv")) && dan===null) res.write(JSON.stringify(structRaspored[j]));
  }
   res.end();

  })

})

app.get("/predmeti",function(req,res){
  

  let structRaspored=new Array();


  fs.readFile("raspored.csv", (err,data)=>{
    if (err){
      var error={"greska":"Datoteka raspored.csv nije kreirana!"};
      res.write(JSON.stringify(error));
      res.end();
    }
    let niz=data.toString().split("\n");
    for (let i=0;i<niz.length-1;i++){  
     
      let info=niz[i].split(",");
      structRaspored[i] = {naziv:info[0], aktivnost:info[1]};
    }
    for (let k=0;k<niz.length-1;k++)
     res.write(JSON.stringify(structRaspored[k]));
   
    res.end();
  });


})


app.post("/saljiPredmete", (req,res)=>{
  let duzina=0;
  let structRaspored=new Array();
  let niz=new Array();

  fs.readFile("raspored.csv", (err,data)=>{
    if (err){
      var error={"greska":"Datoteka raspored.csv nije kreirana!"};
      res.write(JSON.stringify(error));
      res.end();
    }
    let niz=data.toString().split("\n");
  
    for (let i=0;i<niz.length-1;i++){  
     
      let info=niz[i].split(",");
      structRaspored[i] = {naziv:info[0], aktivnost:info[1]};
    }

  
     let dodatak=req.body.ime+","+req.body.aktiv+os.EOL;
    
    fs.appendFile("raspored.csv", dodatak, function (err) {
        if (err) throw err;
        console.log('Dodano!');
      });
    
});



})

app.delete("/saljiPredmete", (req,res)=>{
  let duzina=0;
  let structRaspored=new Array();
  let niz=new Array();

  fs.readFile("raspored.csv", (err,data)=>{
    if (err){
      var error={"greska":"Datoteka raspored.csv nije kreirana!"};
      res.write(JSON.stringify(error));
      res.end();
    }
    let niz=data.toString().split("\n");
    console.log(niz.length);
    for (let i=0;i<niz.length-1;i++){  
     
      let info=niz[i].split(",");
      structRaspored[i] = {naziv:info[0], aktivnost:info[1]};
    }
     let brojac=-1;
    for (let k=0;k<niz.length-1;k++){
        for (let j=k+1;j<niz.length-1;j++){
           if (structRaspored[k].naziv==structRaspored[j].naziv && structRaspored[k].aktivnost==structRaspored[j].aktivnost){
              brojac=k;
              break;
           }
        }
        if (brojac!=-1) break;
    }
    console.log(brojac);
    if (brojac!=-1){
    for (let p=brojac;p<niz.length-1;p++){
      structRaspored[p]=structRaspored[p+1];
    }
  }

     let dodatak="";
    for (let a=0;a<niz.length-2;a++){  //sad ide -2 jer je obrisan onaj sto je pogresno dodan.
     dodatak+=structRaspored[a].naziv+","+structRaspored[a].aktivnost+"\n";
    }
    fs.writeFile("raspored.csv", dodatak, function (err) {
        if (err) throw err;
        console.log('Dodano!');
      });
    


  });
})
db.sequelize.sync().then((req) => { 
app.listen(8080)
});