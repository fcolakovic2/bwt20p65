module.exports = (sequelize, DataTypes) => {

    const Tip = sequelize.define("Tip",{
      naziv: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    }); 
    
    return Tip;
  };