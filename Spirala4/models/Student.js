module.exports = (sequelize, DataTypes) => {

    const Student = sequelize.define("Student",{
      ime: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      indeks: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    }); 
     

    return Student;
  };