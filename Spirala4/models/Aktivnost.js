
let dejt=new Date();
module.exports = (sequelize, DataTypes) => {

    const Aktivnost = sequelize.define("Aktivnost",{
      naziv: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      pocetak: {
        type: DataTypes.FLOAT,
        allowNull: false,
        defaultValue: dejt.getTime(),
      },
      kraj: {
        type: DataTypes.FLOAT,
        allowNull: false,
        defaultValue: dejt.getTime(),
      },
    }); 
  
    return Aktivnost;
  };