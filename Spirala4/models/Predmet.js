module.exports = (sequelize, DataTypes) => {

  const Predmet = sequelize.define("Predmet",{
    naziv: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }); 
  

  return Predmet;
};