let assert = chai.assert;
describe('Raspored', function(){
  describe('#KrajPocetakiBezAktivnosti', function(){

  it('dajTrenutnuAktivnost kada nema trenutne aktivnosti u datom vremenu', function(){
    
    let rasporedTestniUlaz=`BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00\nBWT,predavanje,ponedjeljak,15:00,18:00\nMUR1,predavanje,utorak,09:00,11:00\n
    MUR1-grupa1,vjezbe,srijeda,11:00,12:30\n
    MUR1-grupa2,vjezbe,srijeda,12:30,14:00\n
    RMA,predavanje,ponedjeljak,09:00,12:00\n
    BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30\n
    FWT-grupa2,vjezbe,srijeda,11:00,12:30\n
    FWT-grupa1,vjezbe,srijeda,12:30,14:00\n
    ASP,predavanje,srijeda,09:00,12:00\n
    MUR2,predavanje,cetvrtak,12:00,15:00\n
    FWT,predavanje,cetvrtak,09:00,10:30\n`;
      
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="ASP";
    let time="11-11-2020T13:00:00";

    assert.equal(r.dajTrenutnuAktivnost(predmet,time),"Trenutno nema aktivnosti");

  });

  it('dajTrenutnuAktivnost na početku neke aktivnosti', function(){

    let rasporedTestniUlaz=`BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00\nBWT,predavanje,ponedjeljak,15:00,18:00\nMUR1,predavanje,utorak,09:00,11:00\nMUR1-grupa1,vjezbe,srijeda,11:00,12:30\nMUR1-grupa2,vjezbe,srijeda,12:30,14:00\n
   `;
      
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="MUR1-grupa1";
    let time="11-11-2020T11:00:00";

    assert.equal(r.dajTrenutnuAktivnost(predmet,time),"MUR1-grupa1 90");

 


  });

  it('dajTrenutnuAktivnost na kraju neke aktivnosti', function(){

    let rasporedTestniUlaz=`BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00\nBWT,predavanje,ponedjeljak,15:00,18:00\nMUR1,predavanje,utorak,09:00,11:00\nMUR1-grupa1,vjezbe,srijeda,11:00,12:30\nMUR1-grupa2,vjezbe,srijeda,12:30,14:00\nRMA,predavanje,ponedjeljak,09:00,12:00\n 
    `;
      
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="MUR1-grupa1";
    let time="11-11-2020T12:30:00";

    assert.equal(r.dajTrenutnuAktivnost(predmet,time),"Trenutno nema aktivnosti");
  });


  });

  describe('#OstaliTestniSlučajevi', function(){


 
    it('dajTrenutnuAktivnost kada postoji vježba u datom vremenu ali nije ispravna grupa', function(){
  
        let rasporedTestniUlaz=`BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00\n
    BWT,predavanje,ponedjeljak,15:00,18:00\n
    MUR1,predavanje,utorak,09:00,11:00\n
    MUR1-grupa1,vjezbe,srijeda,11:00,12:30\n
    MUR1-grupa2,vjezbe,srijeda,12:30,14:00\n
    RMA,predavanje,ponedjeljak,09:00,12:00\n
    BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30\n
    FWT-grupa2,vjezbe,srijeda,11:00,12:30\n
    FWT-grupa1,vjezbe,srijeda,12:30,14:00\n
    ASP,predavanje,srijeda,09:00,12:00\n
    MUR2,predavanje,cetvrtak,12:00,15:00\n
    FWT,predavanje,cetvrtak,09:00,10:30`;
      
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="MUR1-grupa5";
    let time="11-11-2020T11:30:00";

    assert.equal(r.dajTrenutnuAktivnost(predmet,time),"Trenutno nema aktivnosti");


    });


    it('dajTrenutnuAktivnost kada postoji vježba u datom vremenu i ispravna je grupa', function(){
  
        let rasporedTestniUlaz=`BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00\nBWT,predavanje,ponedjeljak,15:00,18:00\nMUR1,predavanje,utorak,09:00,11:00\nMUR1-grupa1,vjezbe,srijeda,11:00,12:30\nMUR1-grupa2,vjezbe,srijeda,12:30,14:00\nRMA,predavanje,ponedjeljak,09:00,12:00\nASP,predavanje,srijeda,09:00,12:00`;
      
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="ASP";
    let time="11-11-2020T10:30:00";

    assert.equal(r.dajTrenutnuAktivnost(predmet,time),"ASP 90");


  });

  it('dajPrethodnuAktivnost za slučaj kada je prva prethodna aktivnost vježba sa pogrešnom grupom, tada trebate vratiti aktivnost prije nje', function(){
  
    let rasporedTestniUlaz=`BWT-grupa2,vjezbe,srijeda,13:30,15:00\nBWT,predavanje,ponedjeljak,15:00,18:00\nMUR1,predavanje,utorak,09:00,11:00\nMUR1-grupa1,vjezbe,srijeda,11:00,12:30\nMUR,predavanje,srijeda,09:00,11:00
    `;
      
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="grupa1";  
    let time="11-11-2020T18:01:00";

    assert.equal(r.dajPrethodnuAktivnost(predmet,time),"MUR1-grupa1"); //prva prethodna aktivnost je BWT vjezbe, ali je pogresna grupa. zato
    //vracamo aktivnost prije nje a to je ispravna grupa grupa1 iz MUR-a ( vjezbe ).
 
});



it('dajPrethodnuAktivnost prije prve aktivnosti u ponedjeljak, treba vratiti posljednju aktivnost iz petka, ili četvrtka ako nema aktivnosti u petak itd.', function(){
  
    let rasporedTestniUlaz=`FWT-grupa2,vjezbe,srijeda,11:00,12:30\nFWT-grupa1,vjezbe,srijeda,12:30,14:00\nASP,predavanje,srijeda,09:00,12:00\nMUR2,predavanje,cetvrtak,12:00,15:00\nFWT,predavanje,cetvrtak,09:00,10:30`;

      
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="";
    let time="11-09-2020T18:30:00";

    assert.equal(r.dajPrethodnuAktivnost(predmet,time),"MUR2"); //9. novembar je ponedjeljak. Funkcija vidi da nema nista od aktivnosti
   //taj dan. vraca se na petak, nema ni u petak nista. vraca se na cetvrtak i tu pronadje MUR2 i FWT, ali MUR2 je poslije FWT 
   //odnosno blizi je izvorno poslanom datumu(ponedjeljku) pa se vraca MUR2 ( nekih par sati iza FWT ).
});

it('Salje se grupa1 ali je u narednom danu pa je nastava za danas gotova.',function(){
  
    let rasporedTestniUlaz=`FWT-grupa2,vjezbe,srijeda,11:00,12:30\nFWT-grupa1,vjezbe,srijeda,12:30,14:00\nASP,predavanje,srijeda,09:00,12:00\nMUR2,predavanje,cetvrtak,12:00,15:00\nFWT,predavanje,cetvrtak,09:00,10:30`;
      
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="grupa2";
    let time="11-10-2020T10:30:00";

    assert.equal(r.dajSljedecuAktivnost(predmet,time),"Nastava je gotova za danas");

});

it('ASP ima za pola sata, Sljedeca aktivnost bez ijedne grupe odnosno to je predavanje.',function(){
  
    let rasporedTestniUlaz=`ASP,predavanje,srijeda,09:00,12:00\nMUR2,predavanje,cetvrtak,12:00,15:00\nFWT,predavanje,cetvrtak,09:00,10:30`;
      
    let r=new Raspored(rasporedTestniUlaz);
    let predmet=""; //ovo je grupa
    let time="11-11-2020T08:30:00";

    assert.equal(r.dajSljedecuAktivnost(predmet,time),"ASP 30");
});


it('test za mene...(moja provjera kretanja kroz sedmicu)', function(){
  
    let rasporedTestniUlaz=`FWT-grupa2,vjezbe,srijeda,11:00,12:30\nFWT-grupa1,vjezbe,srijeda,12:30,14:00\nASP,predavanje,srijeda,09:00,12:00\nMUR2,predavanje,cetvrtak,12:00,15:00\nFWT,predavanje,cetvrtak,09:00,10:30`;

      
    let r=new Raspored(rasporedTestniUlaz);
    let predmet="grupa2";
    let time="11-12-2020T18:30:00";

    assert.equal(r.dajPrethodnuAktivnost(predmet,time),"FWT-grupa2"); //datum je cetvrtak, 
    // nema nista za grupu2, vrati trenutni dan na srijedu, tamo ima FWT vjezbe za grupu1, ali prepozna da je to pogresna grupa
    //i zato uzme grupu2 i njihove vjezbe i vrati ih. (iako je ta grupa ISPRED grupe1 i tehnicki je grupa1 prethodna..sto je ispravno!)
});



});



});