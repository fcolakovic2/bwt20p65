Baza se zove wt65st pošto je bilo nekih pitanja da li stavljamo wt ili bwt. Potrebno je samo instalirati mysql2 modul prilikom pokretanja projekta sa "npm install mysql2".

Kada u drugom zadatku dodajemo osobu koja ima i isto ime i isti indeks (dakle identična osoba) ne ispisujem nikakvu grešku u textarea jer u tekstu je spomenut ispis samo errora kod
ponavljanja indeksa sa različitim imenom i tad se i ispisuje error u textarea. U suprotnom se ispiše prazan niz grešaka kao što je traženo tj. [].

Baza se kreira na phpmyadminu, inicijalno prazna. Kreira se kad se pokrene nodejs server.
Implementirano je sve osim mogućnosti da se updateuje grupa kada dodamo istog apsolutno identičnog
studenta, ali sa novom grupom. Ostane mu stara grupa.Možete u commitima pronaći neke pokušaje da se to napravi ali problem je u tome bio što kada je
baza prazna ili ima samo 1 student u njoj, onda u nekim momentima vraća undefined ( uzimao sam id iz studentskegrupe, pa preko toga id iz grupe i 
tako izvlačio naziv grupe i poredio da li ga treba update ). Ali kada je baza prazna onda findAll vraća undefined i pada sve, pa sam to zakomentirao 
u nekim dijelovima koda. Treći je implementiran pomoću novih ruta za one stavke iz forme koje su bile i u prošloj spirali