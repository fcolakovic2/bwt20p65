
function ucitajSortirano(dan,atribut,callback){
    

    var xhr=new XMLHttpRequest();
    xhr.onload = function () {

        if (this.status == 200) {
          callback(this.responseText,null);
        }

        else if (this.status!=200){
            callback(null,this.responseText);
        }

    }
    
    var pomocniString=dan;
    var pomocniString2=atribut;
   
    var string='http://localhost:8080/raspored';
    if ((dan==null || dan=="") && (atribut==null || atribut=="")){
       xhr.open("GET",string,true);
    }

    else  if ((dan==null || dan=="") && (atribut!=null && atribut!="")){ //nema dana ali ima atributa
    xhr.open("GET",string+"?"+"sort="+pomocniString2,true);
    }
    else  if ((dan!=null || dan!="") && (atribut!=null && atribut!="")){ //ima i dana i atributa
    xhr.open("GET",string+"?"+"dan="+pomocniString+"&"+"sort="+pomocniString2,true);
    }
    else  if ((dan!=null && dan!="") && (atribut==null || atribut=="")){ //nema atributa ima samo dana
    xhr.open("GET",string+"?"+"dan="+pomocniString,true);
    }
    

    xhr.send();
}

window.onload=function(){
    ucitajSortirano("","",callback);
}
