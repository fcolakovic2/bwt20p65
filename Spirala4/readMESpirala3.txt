Main.js je glavni js fajl prethodne spirale koji se koristi i u ovoj za pokretanje servera. Treba samo uraditi npm install moment jer nedostaje taj modul, inače to je modul iz prošle spirale 
pa zato nisam dirao ništa ni ubacivao ga lokalno. U ovoj spirali se ne koristi.

Spirala3.js je samo demonstrativno js fajl koji je tu da pokaže kako se može uraditi prvi zadatak, daljnju funkciju nema. Prvi zadatak je implementiran i u main.js jednom linijom koda
koja omogućava učitavanje statičkih klijentskih i html fajlova.

Rute su definisane u main.js vezano za ovu spiralu. Problem jedino može nastupiti kod zadatka4, odnosno posljednjeg zadatka (trebao bi biti zadatak3 po nazivu ali se zove zadatak4).
Tu su mi neke stvari bile nejasne vezano za izradu, konkretno pročitao sam spiralu3 sa WT-a i na taj način htio da uradim. Oni su pravili nove .txt fajlove ali nemaju prethodni
raspored.csv pa sam u svom slučaju iskoristio raspored.csv za taj zadatak. Na get /predmeti dohvatam predmete, na /saljiPredmete ucitavam formu. Izbacio sam iz forme ostale podatke
osim naziva predmeta i aktivnosti jer se to jedino traži, moglo se implementirati i početak i kraj i dan ali je to trivijalno a i nisam s obzirom da se gledalo poređenje po 
duplikatima samo za aktivnosti i predmet(naziv). Također, implementirao sam što se traži, samo malo na svoju ruku jer mi je djelovalo baš nelogično, konkretno nije mi logično da 
provjeravam aktivnost, pa onda provjeravam da li postoji predmet iz te aktivnosti jer naprimjer, ako unesem za aktivnost vježbu ili predavanje, zasigurno će biti neki predmet bar
sa jednom vježbom ili predavanjem. Po tome bi mi ispalo da su sve duplikati u .csv fajlu i na serveru. Dohvatanje sam implementirao kako je i rečeno, onload-u se u <div> učitava
spisak svih predmeta i aktivnosti odvojen zarezom. Što se tiče slanja novih predmeta, to je implementirano klikom na Submit dugme odnosno dugme "Dodaj". Ovdje sam postavio tako da se
uvijek doda ono što korisnik upiše, nebitno postoji li već. Ali onda sam dodao traženu DELETE putanju i funkciju te njen ajax poziv, te kada dodamo neki predmet, klikom na dugme
pored dugmeta "Dodaj" prođe se kroz cijeli niz struktura i obriše se ako se detektuje da smo unijeli duplikat te se to odrazi i na stranici koju vidimo.

Imao sam i 2 alternativna rješenja koje možete vidjeti u mojim prethodnim commitima:

1. Bez dugmeta za provjeru duplikata, dodana funkcija "brisiPredmetDuplikat()" u onload nakon funkcije "upisiPredmet()". Na ovaj način, čim unesemo nešto to se odmah upiše ali se
i automatski pozove brisanje duplikata i tako nemamo nikad duplikat na serveru tj. na stranici ili u .csv fajlu.

2. Bez ikakvog brisanja, jednostavno provjera da li uneseni predmet i njegova aktivnost postoje, ako postoje, ništa se ne uradi odnosno ne može se nikako dodati duplikat pa nema ni
potrebe za DELETE-om. 

Ipak sam odlučio implementirati naknadni delete s obzirom na tekst spirale na WT-u jer sam vidio da smo iz nje dobili ovaj zadatak.

Hvala Vam na razumijevanju i odvojenom vremenu da ovo pročitate.